import { NestMiddleware } from "@nestjs/common";

export class LoggerMiddleware implements NestMiddleware {
    
    use(req: Request, res: Response, next: () => void) {
        console.log('---->', req.method, req.url);
        console.log(new Date());
        
        next();
    }

}