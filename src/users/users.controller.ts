import { Body, Controller, Delete, Get, HttpStatus, Param, Post, Put, Res } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(
        private usersService: UsersService
    ) {}

    @Get()
    getAll(@Res() response) {
        this.usersService.getAll().then( users => {
            response.status(HttpStatus.OK).json(users);
        }).catch( () => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error en la obtención de usuarios'});
        });
    }

    @Post()
    create(@Body() createUser: any, @Res() response) {
        this.usersService.createUser(createUser).then( user => {
            response.status(HttpStatus.CREATED).json(user);
        }).catch( () => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error en la creación del usuario'});
        });
    }

    @Put('/:id')
    update(@Body() updateUser: any, @Res() response, @Param('id') userId) {
        this.usersService.updateUser(userId, updateUser).then( updatedUser => {
            response.status(HttpStatus.OK).json(updatedUser);
        }).catch( () => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error en la edición del usuario'});
        });
    }

    @Delete('/:id')
    delete(@Res() response, @Param('id') userId) {
        this.usersService.deleteUser(userId).then( res => {
            response.status(HttpStatus.OK).json(res);
        }).catch( () => {
            response.status(HttpStatus.FORBIDDEN).json({mensaje: 'Error en la eliminación del usuario'});
        });
    }

}
