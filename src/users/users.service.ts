import { Injectable } from '@nestjs/common';
import * as fs from 'fs';

const fileName = './users.json';
export type User = any;

@Injectable()
export class UsersService {

    users = [];
    
    constructor() {
        try {
            let content = fs.readFileSync(fileName, 'utf-8');
            this.users = JSON.parse(content);
        }
        catch(e) {
            this.users = [];
        }
    }

    writeToFile() {
        fs.writeFileSync(fileName, JSON.stringify(this.users), 'utf-8');
    }

    exists(id) {
        return this.users.findIndex( u => u.id == u ) != -1;
    }

    get nextId() {
        if (this.users.length === 0) return 1;
        let ids = this.users.map( u => u.id );
        
        return 1 + Math.max(...ids);
    }

    async findOne(username: string): Promise<User | undefined> {
        return this.users.find(user => user.username === username);
      }

    async getAll(): Promise<any> {
        return [...this.users];
    }

    async createUser(user: any): Promise<any> {
        user.id = this.nextId;
        this.users.push(user);
        this.writeToFile();

        return user;
    }

    async updateUser(userId: any, updateUser: any): Promise<any> {
        let id = parseInt(userId);
        let index = this.users.findIndex( u => u.id == userId );

        if ( index !== -1 ) {
            this.users[index] = { ...updateUser, id };
            this.writeToFile();
            return { ...this.users[index] };
        }
    }

    async deleteUser(userId: any) {
        // userId = parseInt(userId);
        let index = this.users.findIndex( u => u.id == userId );
        
        if ( index !== -1 ) {
            let userDeleted = this.users.splice(index, 1);
            this.writeToFile();
            return userDeleted;
        }
    }

}
