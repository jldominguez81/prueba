import { HttpException } from '@nestjs/common/exceptions/http.exception';
import { NestMiddleware, HttpStatus, Injectable } from '@nestjs/common';
import { Request, Response, NextFunction } from 'express';
import * as jwt from 'jsonwebtoken';
import { jwtConstants } from './auth/constants';
import { UsersService } from './users/users.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
      private readonly users: UsersService
  ) {}

  async use(req: Request, res: Response, next: NextFunction) {
    const authHeaders = req.headers.authorization;
    if (authHeaders && (authHeaders as string).split(' ')[1]) {
      const token = (authHeaders as string).split(' ')[1];

      let decoded: any;
      try {
        decoded = jwt.verify(token, jwtConstants.secret);
      } catch(e) {
        if (e.message === 'jwt expired') {
            throw new HttpException('Expired Token', HttpStatus.UNAUTHORIZED);
        }
      }
      
      let user = this.users.findOne(decoded.username);

      if (!user) {
        throw new HttpException('User not found.', HttpStatus.UNAUTHORIZED);
      }

      req.user = user;
      next();

    } else {
      throw new HttpException('Not authorized.', HttpStatus.UNAUTHORIZED);
    }
  }
}