import { MiddlewareConsumer, Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { LoggerMiddleware } from './logger.middleware';
import { UsersController } from './users/users.controller';
import { AuthController } from './auth/auth.controller';
import { AuthMiddleware } from './auth.middleware';

@Module({
  imports: [UsersModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware)
      .forRoutes(UsersController, AuthController);
    consumer.apply(AuthMiddleware)
      .forRoutes(
        {path: 'users', method: RequestMethod.GET},
        {path: 'users', method: RequestMethod.PUT},
        {path: 'users', method: RequestMethod.DELETE},
        {path: 'auth/profile', method: RequestMethod.GET},
      )
  }

}
