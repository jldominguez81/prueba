# Prueba NestJS

Prueba de Jose Luis Dóminguez hecha en NestJS con los siguienes puntos cumplidos:

* Crear un proyecto base en Node - Express con un servicio GET llamado /users que devuelva un array de usuarios con la siguiente estructura:
    {
      [
        {
          “username”: “valor”,
          “password”: “valor”
        }
      ]
    }
* Crear un API REST para añadir, actualizar y eliminar nuevos usuarios.
* Crear un servicio para autenticar al usuario (mediante usuario y password). Devolverá un
JWT.
* Crear un middleware para autenticar las peticiones a las API REST (Excepto Login), haciendo uso del JWT.
    - Tampoco se ha habilitado para POST /users, para poder crear usuarios.
* Crear un middleware que muestre por consola la siguiente información de cada petición:
    - Endpoint.
    - fecha y hora.
* Deberá subirse a bitbucket y realizar diversos commit por cada requerimiento.

## Endpoints

Los endpoints dosponibles son los siguientes:
* /users
    - **GET**     getAll
    - **POST**    createUser
* /users/:id `requiere enviar id como parametro`
    - **PUT**     updateUser
    - **DELETE**  deleteUser
* /auth/login
    - **POST**    `enviar json en el body {"username": "nombre", "password": "contraseña"}` | Retorna JWT

## Pasos a seguir para probar proyecto
  - **POST** /users `enviar usuario deseado en el body {"username": "nombre", "password": "contraseña"}`
  - **POST** /auth/login `para obtener JWT para probar el resto de endpoints`

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```
